import { createConnection, getRepository } from "typeorm";
import { Station } from "entities/Station";
import { Bank } from "entities/Bank";
import { Cell } from "entities/Cell";

async function main(): Promise<number> {
  let connection = await createConnection();
  let entityManager = connection.createEntityManager();

  await getRepository(Cell).clear();
  await getRepository(Bank).clear();
  await getRepository(Station).clear();

  let dhiraaguHeadOffice = entityManager.create(Station);
  let upsBank = entityManager.create(Bank);

  upsBank.capacity = 12;
  upsBank.manufacturer = "some_manufacturer";
  upsBank.model = "some_model";

  dhiraaguHeadOffice.name = "DHO";

  await entityManager.save(dhiraaguHeadOffice);

  upsBank.station = dhiraaguHeadOffice;

  await entityManager.save(upsBank);

  for (let i = 0; i < 20; i++) {
    let cell = entityManager.create(Cell);
    cell.bank = upsBank;
    cell.identifier = i;
    await entityManager.save(cell);
  }

  return 0;
}

main()
  .catch(err => {
    console.error(err);
  })
  .then(() => {
    console.log("done seeding!");
  });
