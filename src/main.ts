import { createConnection } from "typeorm";
import { app, bootstrap} from "./server";


async function main(argc: number, argv: string[]){
  await createConnection();
  bootstrap();
  app.listen(2030);
}


main(process.argv.length, process.argv).catch(err => {
  console.error(err);
});