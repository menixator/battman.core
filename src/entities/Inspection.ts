import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  JoinColumn
} from "typeorm";
import { Bank } from "./Bank";
import { Resistance } from "./Resistance";
import { Temperature } from "./Temperature";
import { Voltage } from "./Voltage";

@Entity()
export class Inspection {
  @PrimaryGeneratedColumn()
  id!: number;
  @Column()
  by!: string;
  @Column("datetime")
  inspected!: Date;

  @OneToOne(type => Bank)
  @JoinColumn()
  bank!: Bank;

  @OneToMany(type => Voltage, voltage => voltage.inspection, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  voltages!: Voltage[];

  @OneToMany(type => Resistance, resistance => resistance.inspection, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  resistances!: Resistance[];

  @OneToMany(type => Temperature, temperature => temperature.inspection, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  temperatures!: Temperature[];
}
