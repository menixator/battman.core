import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Bank } from "./Bank";

@Entity()
export class Station {
  @PrimaryGeneratedColumn()
  id!: number;
  @Column({ unique: true })
  name!: string;

  @OneToMany(type => Bank, Bank => Bank.station)
  banks!: Bank[];
}
