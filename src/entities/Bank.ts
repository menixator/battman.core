import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany
} from "typeorm";
import { Station } from "./Station";
import { Cell } from "./Cell";

@Entity()
export class Bank {
  @PrimaryGeneratedColumn()
  id!: number;
  @Column()
  manufacturer!: string;
  @Column()
  model!: string;

  @ManyToOne(type => Station, station => station.banks)
  station!: Station;

  /**
   * Cell capacity in mAh
   */
  @Column()
  capacity!: number;

  @OneToMany(type => Cell, cell => cell.bank)
  cells!: Cell[];
}
