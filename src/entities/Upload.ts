import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn
} from "typeorm";

@Entity()
export class Upload {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  cookie!: string;

  @CreateDateColumn()
  created!: Date;

  @Column()
  uuid!: string;

  @Column()
  filename!: string;
}
