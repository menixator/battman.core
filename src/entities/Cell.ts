import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  OneToMany
} from "typeorm";
import { Bank } from "./Bank";
import { Voltage } from "./Voltage";
import { Resistance } from "./Resistance";
import { Temperature } from "./Temperature";

@Entity()
export class Cell {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Bank, bank => bank.id)
  bank!: Bank;
  @Column()
  identifier!: number;

  @OneToMany(type => Voltage, voltage => voltage.cell)
  voltages!: Voltage[];

  @OneToMany(type => Resistance, resistance => resistance.cell)
  resistances!: Resistance[];

  @OneToMany(type => Temperature, temperature => temperature.cell)
  temperatures!: Temperature[];
}
