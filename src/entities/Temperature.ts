import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  OneToMany
} from "typeorm";
import { Inspection } from "./Inspection";
import { Cell } from "./Cell";

@Entity()
export class Temperature {
  static UNIT: string = "°C";
  static LONG_UNIT: string = "Celcius";

  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Inspection)
  inspection!: Inspection;

  @Column("real")
  value!: number;

  @OneToMany(type => Cell, cell => cell.voltages)
  cell!: Cell;
}
