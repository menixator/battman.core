import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  OneToMany
} from "typeorm";
import { Inspection } from "./Inspection";
import { Cell } from "./Cell";

@Entity()
export class Resistance {
  static UNIT: string = "Ω";
  static LONG_UNIT: string = "Ohms";

  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Inspection)
  inspection!: Inspection;

  @Column("real")
  value!: number;

  @OneToMany(type => Cell, cell => cell.voltages)
  cell!: Cell;
}
