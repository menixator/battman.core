import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  OneToMany
} from "typeorm";
import { Inspection } from "./Inspection";
import { Cell } from "./Cell";

@Entity()
export class Voltage {
  static UNIT: string = "V";
  static LONG_UNIT: string = "Volts";

  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Inspection)
  inspection!: Inspection;

  @Column("real")
  value!: number;

  @OneToMany(type => Cell, cell => cell.voltages)
  cell!: Cell;
}
