import xlsx from "xlsx";
import { Vector } from "tools/Vector";
import { SpreadSheet } from "./SpreadSheet";

export class Cell {
  private src: xlsx.CellObject | null;
  private sheet: SpreadSheet;
  private vector: Vector;

  constructor(sheet: SpreadSheet, vector: Vector) {
    this.sheet = sheet;
    let src = this.sheet.getXSheet()[vector.toString()];

    if (src === undefined) {
      this.src = null;
    } else {
      this.src = src;
    }
    this.vector = vector;
  }

  isNull(): boolean {
    return this.src === null;
  }

  getVector(): Vector {
    return this.vector;
  }

  isString() {
    return !this.isNull() && this.src!.t == "s";
  }

  isNumber() {
    return !this.isNull() && this.src!.t == "n";
  }

  toString() {
    if (this.isNull()) return "";
    if (this.isString()) return this.src!.v as string;
    return "";
  }

  toNumber(): number {
    if (this.isNumber()) return this.src!.v as number;
    return 0;
  }
}
