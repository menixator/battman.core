import { Vector } from "tools/Vector";
import xlsx from "xlsx";
import { Cell } from "./Cell";

export class SpreadSheet {
  private xsheet: xlsx.Sheet;
  private start: Vector;
  private end: Vector;
  private name: string;

  constructor(xsheet: xlsx.Sheet, name: string) {
    this.name = name;

    this.xsheet = xsheet;
    let [startRangeStr, endRangeStr] = xsheet["!ref"]!.split(":");
    this.start = Vector.fromString(startRangeStr);
    this.end = Vector.fromString(endRangeStr);
  }

  public getXSheet(): xlsx.Sheet {
    return this.xsheet;
  }

  public getName(): string {
    return this.name;
  }

  *cells(): IterableIterator<[Vector, Cell]> {
    for (let key in this.xsheet) {
      if (!{}.hasOwnProperty.call(this.xsheet, key)) continue;
      if (!key.match(/^[A-Z]+\d+$/)) continue;
      if (this.xsheet[key] == undefined) continue;
      yield [Vector.fromString(key), new Cell(this, Vector.fromString(key))];
    }
  }

  inRange(vector: Vector) {
    return (
      vector.greaterThanOrEqualTo(this.start) &&
      this.end.greaterThanOrEqualTo(vector)
    );
  }

  has(vector: Vector) {
    return (
      {}.hasOwnProperty.call(this.xsheet, vector.toString()) &&
      this.xsheet[vector.toString()] != undefined
    );
  }

  get(vector: Vector): Cell {
    return this.has(vector) ? new Cell(this, vector) : new Cell(this, vector);
  }

  getString(vector: Vector) {
    return this.get(vector).toString();
  }
}
