import { Vector } from "tools/Vector";

export class StringField {
  public label: string;
  public labelVector: Vector;
  public valueVector: Vector;

  constructor(label: string, labelLocation: String, valueLocation: String) {
    this.label = label;
    this.labelVector = Vector.fromString(labelLocation);
    this.valueVector = Vector.fromString(valueLocation);
  }
}
