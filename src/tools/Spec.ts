import { StringField } from "tools/StringField";

export namespace Spec {
  export const station = new StringField("Station: ", "A1", "B1");
  export const inspectedBy = new StringField("Inspected by:", "A2", "B2");
  export const lastInspectedBy = new StringField(
    "Last Inspaction done by:",
    "A3",
    "B3"
  );
  export const dateInstalled = new StringField("Date Installed:", "E1", "F1");
  export const inspectionDate = new StringField(
    "Date of Inspection:",
    "E2",
    "F2"
  );
  export const lastInspectionDate = new StringField(
    "Last Inspaction Date:",
    "E3",
    "F3"
  );
  export const numberOfStrings = new StringField("No. of strings:", "E4", "F4");
  export const batteryManufacturer = new StringField(
    "Battery Manufacturer:",
    "H1",
    "I1"
  );
  export const batteryModel = new StringField("Battery Model:", "H2", "I2");
  export const batteryCellType = new StringField(
    "Battery/Cell Type:",
    "H3",
    "I3"
  );
  export const batteryQTTYPerString = new StringField(
    "Batt. Qty/string:",
    "H4",
    "I4"
  );
  export const periodFrom = new StringField("Period From:", "L1", "M1");
  export const periodTo = new StringField("Period To:", "L2", "M2");
  export const cellVoltage = new StringField("Cell Volt (V):", "L3", "M3");
  export const cellCapacity = new StringField("Cell Cap (Ah):", "L4", "M4");
}
