export const FIRST_COLUMN_CHAR_CODE = 65;
export const COLUMN_SPAN_RANGE_SIZE = 26;

export class Vector {
  static fromString(pos: String): Vector {
    let str = pos.toUpperCase().trim();
    if (!str.match(/^[A-Z]+\d+$/)) {
      throw new TypeError(`'${pos}' is not a valid position.`);
    }
    let column = str.match(/^[A-Z]+/)!;
    let row = str.match(/\d+$/)!;
    let x = column[0]
      .split("")
      .reverse()
      .reduce((prev: number, char: string, index: number): number => {
        return (
          prev +
          (COLUMN_SPAN_RANGE_SIZE * index +
            (char.charCodeAt(0) - FIRST_COLUMN_CHAR_CODE))
        );
      }, 0);
    let y = parseInt(row[0], 10);
    return new Vector(x, y);
  }

  constructor(public x: number, public y: number) {}

  add(vector: Vector) {
    return new Vector(this.x + vector.x, this.y + vector.y);
  }

  diff(vector: Vector) {
    return new Vector(this.x - vector.x, this.y - vector.y);
  }

  greaterThan(vector: Vector) {
    return this.x > vector.x && this.y > vector.y;
  }

  greaterThanOrEqualTo(vector: Vector) {
    return this.x >= vector.x && this.y >= vector.y;
  }

  toString() {
    let i = this.x;
    let column = [];
    while (i >= 0) {
      column.push(
        String.fromCharCode(
          FIRST_COLUMN_CHAR_CODE + (i % COLUMN_SPAN_RANGE_SIZE)
        )
      );
      i -= COLUMN_SPAN_RANGE_SIZE;
    }

    return column.join("") + this.y.toString();
  }
}
