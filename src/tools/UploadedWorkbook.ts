import xlsx from "xlsx";
import { SpreadSheet } from "./SpreadSheet";
import { Vector } from "tools/Vector";
import { Spec } from "./Spec";
import { StringField } from "./StringField";

type SheetType = "resistance" | "voltage" | "temperature";

interface DataUnit {
  bankId: number;
  cellId: number;
  value: number;
}

interface Metadata {
  station: string;
  inspectedBy: string;
  lastInspectedBy: string;

  dateInstalled?: string;
  inspectionDate: string;
  lastInspectionDate: string;

  batteryManufacturer: string;
  batteryModel: string;
  batteryCellType: string;
  batteryQTTYPerString: string;

  periodFrom: string;
  periodTo: string;
  cellVoltage: string;
  cellCapacity: string;
  numberOfStrings: string;
}

interface Sheet<T extends SheetType> {
  metadata: Metadata;
  type: T;
  data: DataUnit[];
}

interface InspectionReport {
  metadata: Metadata;
  resistance: Sheet<"resistance">;
  voltage: Sheet<"voltage">;
  temperature: Sheet<"temperature">;
}

export class UploadedWorkbook {
  private buffer: Buffer;
  private workbook: xlsx.WorkBook;
  private report: InspectionReport;

  public warnings: String[] = [];

  constructor(buffer: Buffer) {
    this.buffer = buffer;
    this.workbook = xlsx.read(this.buffer);
    this.report = {} as InspectionReport;
  }

  private static sanitize(str: string) {
    return str
      .toLowerCase()
      .trim()
      .replace(/[^A-Z\d]{1,}|:$/g, "");
  }

  private static parseSheetType(str: string): SheetType | null {
    if (str.match(/Resistance/i)) {
      return "resistance";
    }

    if (str.match(/Voltage/i)) {
      return "voltage";
    }

    if (str.match(/Temperature/i)) {
      return "temperature";
    }
    return null;
  }

  public getInspectionReport() {
    return this.report;
  }

  public parse() {
    for (let sheetName of this.workbook.SheetNames) {
      let sheetType = UploadedWorkbook.parseSheetType(sheetName);
      if (sheetType == null) {
        this.warnings.push(
          `${sheetName}: Did not match any pre-defined sheet types`
        );
        continue;
      }

      if (this.workbook.Sheets[sheetName]["!ref"] == undefined) {
        this.warnings.push(
          `${sheetName}: Range was not parsed correctly. Ignoring.`
        );
        continue;
      }

      this.parseSheet(
        sheetType,
        new SpreadSheet(this.workbook.Sheets[sheetName], sheetName)
      );
    }

    // Resistance/Temperature or Voltage sheet might be missing.
    for (let key in ["resistance", "temperature", "voltage"]) {
      if (!{}.hasOwnProperty.call(this.report, key)) {
        this.warnings.push(
          `Could not find ${key.substr(0, 1).toUpperCase()}${key.substr(
            1
          )} sheet`
        );
      }
    }
  }

  private parseSheet<T extends SheetType>(type: T, xsheet: SpreadSheet) {
    let sheet: Sheet<T> = {} as Sheet<T>;
    sheet.type = type;
    sheet.metadata = {} as Metadata;
    sheet.data = [];
    this.collectData(sheet, xsheet);
    switch (type) {
      case "resistance":
        this.report.resistance = sheet as Sheet<"resistance">;
        break;

      case "temperature":
        this.report.temperature = sheet as Sheet<"temperature">;
        break;

      case "voltage":
        this.report.voltage = sheet as Sheet<"voltage">;
        break;
    }
  }

  private parseField(xsheet: SpreadSheet, field: StringField, offset: Vector) {
    let label = xsheet.get(field.labelVector.add(offset));

    if (!label.isString()) {
      this.warnings.push(
        `${xsheet.getName()}: Expected a string at ${label
          .getVector()
          .toString()}`
      );
      return "";
    }

    if (
      UploadedWorkbook.sanitize(label.toString()) !=
      UploadedWorkbook.sanitize(field.label)
    ) {
      this.warnings.push(
        `${xsheet.getName()}: Expected the string '${
          field.label
        }' at ${label.getVector().toString()}`
      );
      return "";
    }
    let data = xsheet.get(field.valueVector.add(offset));
    return data.isNull() ? "" : data.toString();
  }

  private collectData<T extends SheetType>(
    sheet: Sheet<T>,
    xsheet: SpreadSheet
  ) {
    let metadata = sheet.metadata;

    let offset = new Vector(0, 0);

    let station = xsheet.get(Spec.station.labelVector);

    // Find the station name if it cannot be found.
    if (
      station.isNull() ||
      !station.isString() ||
      UploadedWorkbook.sanitize(station.toString()) !=
        UploadedWorkbook.sanitize(Spec.station.label)
    ) {
      let found = false;
      for (let [vector, cell] of xsheet.cells()) {
        if (cell.isNull() || !cell.isString()) continue;
        if (
          UploadedWorkbook.sanitize(cell.toString()) ==
          UploadedWorkbook.sanitize(Spec.station.label)
        ) {
          found = true;
          offset = vector.diff(Spec.station.labelVector);
          break;
        }
      }
      if (!found) {
        this.warnings.push(`Could not parse the data from ${xsheet.getName()}`);
      }
    }
    metadata.station = this.parseField(xsheet, Spec.station, offset);
    metadata.inspectedBy = this.parseField(xsheet, Spec.inspectedBy, offset);
    metadata.lastInspectedBy = this.parseField(
      xsheet,
      Spec.lastInspectedBy,
      offset
    );
    metadata.dateInstalled = this.parseField(
      xsheet,
      Spec.dateInstalled,
      offset
    );
    metadata.inspectionDate = this.parseField(
      xsheet,
      Spec.inspectionDate,
      offset
    );
    metadata.lastInspectionDate = this.parseField(
      xsheet,
      Spec.lastInspectionDate,
      offset
    );
    metadata.numberOfStrings = this.parseField(
      xsheet,
      Spec.numberOfStrings,
      offset
    );
    metadata.batteryManufacturer = this.parseField(
      xsheet,
      Spec.batteryManufacturer,
      offset
    );
    metadata.batteryModel = this.parseField(xsheet, Spec.batteryModel, offset);
    metadata.batteryCellType = this.parseField(
      xsheet,
      Spec.batteryCellType,
      offset
    );
    metadata.batteryQTTYPerString = this.parseField(
      xsheet,
      Spec.batteryQTTYPerString,
      offset
    );
    metadata.periodFrom = this.parseField(xsheet, Spec.periodFrom, offset);
    metadata.periodTo = this.parseField(xsheet, Spec.periodTo, offset);
    metadata.cellVoltage = this.parseField(xsheet, Spec.cellVoltage, offset);
    metadata.cellCapacity = this.parseField(xsheet, Spec.cellCapacity, offset);

    let dataPtr = Spec.station.labelVector.add(offset).add(new Vector(0, 7));

    while (xsheet.inRange(dataPtr)) {
      while (
        xsheet.inRange(dataPtr) &&
        (xsheet.get(dataPtr).isNull() ||
          !xsheet.get(dataPtr).isString() ||
          !xsheet
            .get(dataPtr)
            .toString()
            .trim()
            .match(/^bank(\s*)(\d+)/i))
      ) {
        dataPtr = dataPtr.add(new Vector(0, 1));
      }

      if (!xsheet.inRange(dataPtr)) break;
      let bankId = parseInt(
        xsheet
          .get(dataPtr)
          .toString()
          .trim()
          .toLowerCase()
          .match(/bank\s*(\d+)/)![1],
        10
      );

      dataPtr = dataPtr.add(new Vector(0, 2));

      while (!xsheet.get(dataPtr).isNull() && xsheet.inRange(dataPtr)) {
        let cellId = xsheet.get(dataPtr).toNumber();

        let valueCell = xsheet.get(dataPtr.add(new Vector(1, 0)));
        let value: number = 0;

        if (!valueCell.isNumber()) {
          this.warnings.push(
            `${xsheet.getName()}: Expected a number at ${valueCell
              .getVector()
              .toString()} for Bank#${bankId}:Cell#${cellId}`
          );
          value = 0;
        } else {
          value = valueCell.toNumber();
        }
        let dataUnit: DataUnit = {
          bankId: bankId,
          cellId: cellId,
          value
        };
        sheet.data.push(dataUnit);
        dataPtr = dataPtr.add(new Vector(0, 1));
      }
    }
  }
}
