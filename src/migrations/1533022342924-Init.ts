import { MigrationInterface, QueryRunner } from "typeorm";

export class Init1533022342924 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "station" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, CONSTRAINT "UQ_0c4cf3382f77cddaf304687e95c" UNIQUE ("name"))`
    );
    await queryRunner.query(
      `CREATE TABLE "resistance" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `CREATE TABLE "temperature" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `CREATE TABLE "inspection" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "by" varchar NOT NULL, "inspected" datetime NOT NULL)`
    );
    await queryRunner.query(
      `CREATE TABLE "voltage" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `CREATE TABLE "cell" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "identifier" integer NOT NULL, "bankId" integer)`
    );
    await queryRunner.query(
      `CREATE TABLE "bank" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "manufacturer" varchar NOT NULL, "model" varchar NOT NULL, "capacity" integer NOT NULL, "stationId" integer)`
    );
    await queryRunner.query(
      `CREATE TABLE "temporary_resistance" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer, CONSTRAINT "FK_00a52a1bdb9fc92478caaa3c165" FOREIGN KEY ("inspectionId") REFERENCES "inspection" ("id"))`
    );
    await queryRunner.query(
      `INSERT INTO "temporary_resistance"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "resistance"`
    );
    await queryRunner.query(`DROP TABLE "resistance"`);
    await queryRunner.query(
      `ALTER TABLE "temporary_resistance" RENAME TO "resistance"`
    );
    await queryRunner.query(
      `CREATE TABLE "temporary_temperature" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer, CONSTRAINT "FK_be2a5ce726af56a66ce75d2623d" FOREIGN KEY ("inspectionId") REFERENCES "inspection" ("id"))`
    );
    await queryRunner.query(
      `INSERT INTO "temporary_temperature"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "temperature"`
    );
    await queryRunner.query(`DROP TABLE "temperature"`);
    await queryRunner.query(
      `ALTER TABLE "temporary_temperature" RENAME TO "temperature"`
    );
    await queryRunner.query(
      `CREATE TABLE "temporary_voltage" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer, CONSTRAINT "FK_a09d24b4865a9f37f97c342116f" FOREIGN KEY ("inspectionId") REFERENCES "inspection" ("id"))`
    );
    await queryRunner.query(
      `INSERT INTO "temporary_voltage"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "voltage"`
    );
    await queryRunner.query(`DROP TABLE "voltage"`);
    await queryRunner.query(
      `ALTER TABLE "temporary_voltage" RENAME TO "voltage"`
    );
    await queryRunner.query(
      `CREATE TABLE "temporary_cell" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "identifier" integer NOT NULL, "bankId" integer, CONSTRAINT "FK_29461d768929d07a69ee0547d49" FOREIGN KEY ("bankId") REFERENCES "bank" ("id"))`
    );
    await queryRunner.query(
      `INSERT INTO "temporary_cell"("id", "identifier", "bankId") SELECT "id", "identifier", "bankId" FROM "cell"`
    );
    await queryRunner.query(`DROP TABLE "cell"`);
    await queryRunner.query(`ALTER TABLE "temporary_cell" RENAME TO "cell"`);
    await queryRunner.query(
      `CREATE TABLE "temporary_bank" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "manufacturer" varchar NOT NULL, "model" varchar NOT NULL, "capacity" integer NOT NULL, "stationId" integer, CONSTRAINT "FK_739c85e2affb7fe0a80cae7d6f2" FOREIGN KEY ("stationId") REFERENCES "station" ("id"))`
    );
    await queryRunner.query(
      `INSERT INTO "temporary_bank"("id", "manufacturer", "model", "capacity", "stationId") SELECT "id", "manufacturer", "model", "capacity", "stationId" FROM "bank"`
    );
    await queryRunner.query(`DROP TABLE "bank"`);
    await queryRunner.query(`ALTER TABLE "temporary_bank" RENAME TO "bank"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bank" RENAME TO "temporary_bank"`);
    await queryRunner.query(
      `CREATE TABLE "bank" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "manufacturer" varchar NOT NULL, "model" varchar NOT NULL, "capacity" integer NOT NULL, "stationId" integer)`
    );
    await queryRunner.query(
      `INSERT INTO "bank"("id", "manufacturer", "model", "capacity", "stationId") SELECT "id", "manufacturer", "model", "capacity", "stationId" FROM "temporary_bank"`
    );
    await queryRunner.query(`DROP TABLE "temporary_bank"`);
    await queryRunner.query(`ALTER TABLE "cell" RENAME TO "temporary_cell"`);
    await queryRunner.query(
      `CREATE TABLE "cell" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "identifier" integer NOT NULL, "bankId" integer)`
    );
    await queryRunner.query(
      `INSERT INTO "cell"("id", "identifier", "bankId") SELECT "id", "identifier", "bankId" FROM "temporary_cell"`
    );
    await queryRunner.query(`DROP TABLE "temporary_cell"`);
    await queryRunner.query(
      `ALTER TABLE "voltage" RENAME TO "temporary_voltage"`
    );
    await queryRunner.query(
      `CREATE TABLE "voltage" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `INSERT INTO "voltage"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "temporary_voltage"`
    );
    await queryRunner.query(`DROP TABLE "temporary_voltage"`);
    await queryRunner.query(
      `ALTER TABLE "temperature" RENAME TO "temporary_temperature"`
    );
    await queryRunner.query(
      `CREATE TABLE "temperature" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `INSERT INTO "temperature"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "temporary_temperature"`
    );
    await queryRunner.query(`DROP TABLE "temporary_temperature"`);
    await queryRunner.query(
      `ALTER TABLE "resistance" RENAME TO "temporary_resistance"`
    );
    await queryRunner.query(
      `CREATE TABLE "resistance" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" real NOT NULL, "inspectionId" integer)`
    );
    await queryRunner.query(
      `INSERT INTO "resistance"("id", "value", "inspectionId") SELECT "id", "value", "inspectionId" FROM "temporary_resistance"`
    );
    await queryRunner.query(`DROP TABLE "temporary_resistance"`);
    await queryRunner.query(`DROP TABLE "bank"`);
    await queryRunner.query(`DROP TABLE "cell"`);
    await queryRunner.query(`DROP TABLE "voltage"`);
    await queryRunner.query(`DROP TABLE "inspection"`);
    await queryRunner.query(`DROP TABLE "temperature"`);
    await queryRunner.query(`DROP TABLE "resistance"`);
    await queryRunner.query(`DROP TABLE "station"`);
  }
}
