import bodyparser from "body-parser";
import { O_RDWR } from "constants";
import { Bank } from "entities/Bank";
import { Cell } from "entities/Cell";
import { Inspection } from "entities/Inspection";
import { Resistance } from "entities/Resistance";
import { Station } from "entities/Station";
import { Temperature } from "entities/Temperature";
import { Upload } from "entities/Upload";
import { Voltage } from "entities/Voltage";
import express, { Express } from "express";
import { access, mkdirSync, Stats, statSync, unlink } from "fs";
import joi from "joi";
import morgan from "morgan";
import multer from "multer";
import { tmpdir } from "os";
import { join } from "path";
import { EntityMetadata, getConnection } from "typeorm";
import uuid from "uuid/v4";
import cookieparser from "cookie-parser";
const MIN_LIMIT = 10;
const MAX_LIMIT = 20;

export const app: Express = express();

app.use(morgan("short"));
app.use(cookieparser());

export interface RestError {
  path?: string;
  message: string;
}
export interface RestResponse {
  errors: RestError[] | null;
  data: any;
}

const includeValidator = joi
  .array()
  .allow(joi.string())
  .default([]);

const documentCollectionSchema = joi.object({
  limit: joi
    .number()
    .min(MIN_LIMIT)
    .max(MAX_LIMIT)
    .default(MAX_LIMIT),

  offset: joi.number().default(0),
  include: includeValidator
});

const documentSchema = joi.object({
  include: includeValidator
});

type ModelTypes =
  | typeof Bank
  | typeof Cell
  | typeof Inspection
  | typeof Resistance
  | typeof Station
  | typeof Temperature
  | typeof Voltage;

const api = express.Router();

const has = (obj: any, str: string) => ({}.hasOwnProperty.call(obj, str));

export const TMP_DIR = join(tmpdir(), "battman");

// Create the temporary directory for file uploads.

let stats: Stats;

try {
  stats = statSync(TMP_DIR);
  if (!stats.isDirectory()) {
    console.error(
      `The generated temporary directory: ${TMP_DIR} exists and is not a directory`
    );
  }
} catch (err) {
  if (err.code == "ENOENT") {
    mkdirSync(TMP_DIR);
  }
}

const datasheetUpload = multer({
  storage: multer.diskStorage({
    destination: TMP_DIR,
    filename(req, file, callback) {
      return callback(null, uuid());
    }
  })
});

app.use((req, res, next) => {
  let _id: { value: string; lastrefreshed: number };

  if (!{}.hasOwnProperty.call(req.cookies, "_id")) {
    _id = {
      value: Date.now().toString(16),
      lastrefreshed: Date.now()
    };
    // add a new cookie if it isnt there.
    res.cookie("_id", JSON.stringify(_id), {
      httpOnly: true,
      expires: new Date(Date.now() + 15 * 24 * 60 * 60 * 1000)
    });
  } else {
    // parse the cookie if it is there.
    _id = <{ value: string; lastrefreshed: number }>(
      cookieparser.JSONCookie(req.cookies._id)
    );
    if (
      typeof _id != "object" ||
      !has(_id, "value") ||
      !has(_id, "lastrefreshed") ||
      typeof _id.lastrefreshed != "number" ||
      typeof _id.value != "number"
    ) {
      _id = {
        value: Date.now().toString(16),
        lastrefreshed: Date.now()
      };

      res.cookie("_id", JSON.stringify(_id), {
        httpOnly: true,
        expires: new Date(Date.now() + 15 * 24 * 60 * 60 * 1000)
      });
    } else if (Date.now() - _id.lastrefreshed > 10 * 24 * 60 * 60 * 1000) {
      _id = {
        value: _id.value,
        lastrefreshed: Date.now()
      };

      res.cookie("_id", JSON.stringify(_id), {
        httpOnly: true,
        expires: new Date(Date.now() + 15 * 24 * 60 * 60 * 1000)
      });
    }
    req.cookies._id = _id;
  }
  next();
});

export function bootstrap(): void {
  [Bank, Cell, Inspection, Resistance, Station, Temperature, Voltage].forEach(
    (entityClass: ModelTypes) => {
      let connection = getConnection();
      let tableName = connection.getMetadata(entityClass).tableName;

      // Filepond conformant endpoints.
      // The fieldname should be properly set.
      api.post(
        "/datasheet/upload",
        datasheetUpload.single("datasheet"),
        async (req, res, next) => {
          await connection.getRepository(Upload).create({
            cookie: req.cookies._id.value,
            filename: req.file.originalname,
            uuid: req.file.filename
          });

          return res.status(200).send(req.file.filename);
        }
      );

      api.delete(
        "/datasheet/delete",
        bodyparser.text(),
        async (req, res, next) => {
          if (req.body == undefined || req.body.length === 0) {
            return res.status(422).send("ERR: malformed request");
          }

          let id = req.body;
          let file = await connection.getRepository(Upload).findOne({
            uuid: id
          });

          if (file == undefined || file.cookie != req.cookies._id.value)
            return res.status(404).send("ERR: file not found");

          let filepath = join(TMP_DIR, id);
          console.log(`trying to delete: ${filepath}`);
          access(filepath, O_RDWR, err => {
            if (err) return res.status(404).send("ERR: file not found");
            unlink(filepath, async err => {
              if (err) {
                return res.status(522).send("ERR: failed to delete upload");
              }

              await connection.getRepository(Upload).delete({
                uuid: id
              });

              return res.status(200).send("");
            });
          });
        }
      );

      api.get(`/${tableName}s`, async (req, res, next) => {
        let { error, value } = joi.validate(
          req.query,
          documentCollectionSchema
        );
        if (error != null) {
          return res.status(422).json(<RestResponse>{
            errors: error.details.map(err => ({
              message: err.message,
              path: "/" + err.path.join("/")
            })),
            data: null
          });
        }

        let entityMetadata = connection.getMetadata(entityClass);

        let invalidIncludes = getInvalidIncludes(entityMetadata, value.include);

        if (invalidIncludes.length > 0) {
          return res.status(422).json(<RestResponse>{
            errors: invalidIncludes.map(invalidInclude => ({
              message: `Invalid include property '${invalidInclude}'`
            }))
          });
        }

        let entities = await connection.getRepository(entityClass).find({
          skip: value.offset,
          take: value.limit,
          relations: value.include
        });

        return res.status(200).json(<RestResponse>{
          errors: null,
          data: entities
        });
      });

      api.get(`/${tableName}s/:id`, async (req, res, next) => {
        let id = parseInt(req.params.id);

        if (Number.isNaN(id)) {
          return res.status(422).json(<RestResponse>{
            errors: [
              {
                message: "Id should be a number"
              }
            ]
          });
        }

        let { error, value } = joi.validate(req.query, documentSchema);
        if (error != null) {
          return res.status(422).json(<RestResponse>{
            errors: error.details.map(err => ({
              message: err.message,
              path: "/" + err.path.join("/")
            })),
            data: null
          });
        }
        let entityMetadata = connection.getMetadata(entityClass);
        let invalidIncludes = getInvalidIncludes(entityMetadata, value.include);

        if (invalidIncludes.length > 0) {
          return res.status(422).json(<RestResponse>{
            errors: invalidIncludes.map(invalidInclude => ({
              message: `Invalid include property '${invalidInclude}'`
            }))
          });
        }

        let entity = await connection.getRepository(entityClass).findOne({
          relations: value.include,
          where: {
            id
          }
        });

        if (!entity) {
          return res.status(404).json(<RestResponse>{
            errors: [
              {
                message: `Couldn't find an entity with with the requested id '${id}'`
              }
            ]
          });
        }

        return res.status(200).json(<RestResponse>{
          errors: null,
          data: entity
        });
      });
    }
  );
}

function getInvalidIncludes(
  entityMetadata: EntityMetadata,
  includes: string[]
): string[] {
  let relationNames = entityMetadata.relations.map(rel => rel.propertyName);
  let invalid: string[] = [];

  for (let i = 0; i < includes.length; i++) {
    if (relationNames.indexOf(includes[i]) == -1) {
      invalid.push(includes[i]);
    }
  }
  return invalid;
}

app.use("/api/v1/", api);
